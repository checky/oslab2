#include "string.h"
#include "assert.h"
#include "common.h"
typedef struct {
	char *a0; /* pointer to first homed integer argument */
	int offset; /* byte offset of next parameter */
} va_list;
#define _INTSIZEOF(n) ((sizeof(n)+sizeof(int)-1)&~(sizeof(int) - 1) )
#define va_arg(ap,t) ( *(t *)((ap += _INTSIZEOF(t)) - _INTSIZEOF(t)) )
#define va_start(ap,v) ( ap = (va_list)&v + _INTSIZEOF(v) )
#define va_end(ap) ( ap = (va_list)0 )
/*
 * use putchar_func to print a string
 *   @putchar_func is a function pointer to print a character
 *   @format is the control format string (e.g. "%d + %d = %d")
 *   @data is the address of the first variable argument
 * please implement it.
 */

void putstring(void (*putchar_func)(char),char *s){
	for (; *s; s ++) putchar_func(*s);
}
int
vfprintf(void (*putchar_func)(char), const char *format, void **data) 
{
	const char *mainstr=format;
	for(;*mainstr;mainstr++){
		switch(*mainstr)
		{
			case '%':
			{
				mainstr++;
				switch(*mainstr){
					case 's':putstring(putchar_func,(char *)*data);data++;break;
					case 'd':putstring(putchar_func,itoa(*(int *)data,10));data++;break;
					case 'x':putstring(putchar_func,utoa(*(int *)data,16));data++;break;
					case 'c':putchar_func(*(char *)data);data++;break;
				}
				break;
			}
			default:putchar_func(*mainstr);break;
		}
	}
	return 0;
}