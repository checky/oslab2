#include "thread_test.h"
void thread_ex1() { 
    int x = 0;
    while(1) {
        if(x <=0) {
            x=10000;
            printk("a");
        }
        x --;
    }
}
void thread_ex2() { 
    int x = 0;
    while(1) {
        if(x <=0) {
            x=10000;
            printk("b");
        }
        x --;
    }
}
void thread_ex3() { 
    int x = 0;
    while(1) {
        if(x <= 0) {
            x=10000;
            printk("c");
        }
        x --;
    }
}
void thread_testfunc(){
	pcb1=create_kthread(thread_ex1);
	pcb2=create_kthread(thread_ex2);
	pcb3=create_kthread(thread_ex3);
	wakeup(pcb3);wakeup(pcb2);wakeup(pcb1);
}