//#include "syscall.h"
#include "x86.h"
#include "debug.h"
#include "kernel.h"
#include "string.h"
#include "process.h"

enum
{
	SYS_cputs = 0,
	SYS_cgetc,
	SYS_getpid,
	SYS_pcb_destroy,
	SYS_page_alloc,
	SYS_page_map,
	SYS_page_unmap,
	SYS_exofork,
	SYS_set_status,
	SYS_set_trapframe,
	SYS_set_pgfault_upcall,
	SYS_yield,
	SYS_ipc_try_send,
	SYS_ipc_recv,
	NSYSCALLS
};

static void sys_cputs(const char *s,size_t len)
{
	//printk("HERE in kernel sys_cputs %s %d\n",__FILE__,__LINE__);
	printk("%s",s);
}

int syscall(uint32_t syscallno,uint32_t a1,uint32_t a2,uint32_t a3,uint32_t a4,int32_t a5)
{
	cli();
	//printk("HERE in kernel syscall %s %d\n",__FILE__,__LINE__);
	int res=0;
	switch(syscallno)
	{
		case SYS_cputs:sys_cputs((char*)a2,a3);break;
		case SYS_exofork:sys_fork();break;
	}
	/* Lab2 code here   根据系统调用号，调用不同的系统调用处理程序
	 */
	return res;
}
