#include "kernel.h"
#include "x86.h"
#include "vm.h"
#include "irq.h"
#include "process.h"
#include "sched.h"
#include "timer.h"
extern int pid_pool;
PCB *idle;
void init_proc(void)
{
	pid_pool=0;
	/*调用进程池初始化函数*/
	PCBPool_init();
	idle=create_kthread(0);
	/*初始化就绪队列与阻塞队列*/
	queue_init();
	current=idle;
}
extern void thread_ex1();
void
os_init(void) {
   	 /*Reset the GDT.User processes in this systems run
	  in Ring 3,they have their own virtual address space.
	  Therefore,the old GDT located in physical address 0x7c00 
	  cannot be used again.*/
	init_seg();
	/*Initialize the serial port.After that,you can use printk()*/
	init_debug();
	/*Set up interrupt and exception handlers.*/
	init_idt();
	/*Initialize the intel 8259 PIC*/
	init_i8259();
	/*Initialize process.*/
	i386_vm_init();

	init_proc();
	enter_process_userspace();
	thread_testfunc();
	init_timer();

	sti();
	while(1)	{wait_intr();printk("back to idle");}
    	/*jump to the user space*/
	/*This context now becomes the idle process.*/
}

void
entry(void) {
	init_kvm();
	void(*volatile next)(void) = os_init;
	asm volatile("addl %0, %%esp" : : ""(KOFFSET));
	next();
	panic("init code should never return");
}




