#include "x86.h"
#include "adt/list.h"
#include "kernel.h"
#include "process.h"
#include "thread_test.h"
#define SECTORS_SIZE 512
#define USTACKTOP 0xbfffffff
#define ELF_MAGIC 0x464C457FU
#define ELF_PROG_LOAD 1

PCB *pcb_free;

int pid_pool;
//此函数可用于测试
void print_stack(TrapFrame *tf)
{
	printk("edi = 0x%x\n",tf->edi);
	printk("esi = 0x%x\n",tf->esi);
	printk("ebp = 0x%x\n",tf->ebp);
	printk("esp_ = 0x%x\n",tf->esp_);
	printk("ebx = 0x%x\n",tf->ebx);
	printk("edx = 0x%x\n",tf->edx);
	printk("ecx = 0x%x\n",tf->ecx);
	printk("eax = 0x%x\n",tf->eax);
//	printk("gs = %d\n",tf->gs);
//	printk("fs = %d\n",tf->fs);
	printk("es = %d\n",tf->es);
	printk("ds = %d\n",tf->ds);
	printk("irq = %d\n",tf->irq);
	printk("err = %d\n",tf->err);
	printk("eip = 0x%x\n",tf->eip);
	printk("eax = 0x%x\n",tf->eax);


	printk("cs = %d\n",tf->cs);
	printk("eflags = 0x%x\n",tf->eflags);
	printk("esp = 0x%x\n",tf->esp);
	printk("ss = %d\n",tf->ss);
}


//TrapFrame init
//内核线程堆栈结构初始化，用户线程的初始化可以参考
void TrapFrame_init(TrapFrame *tf)
{
	tf -> ds = KSEL(SEG_KDATA);
	tf -> es = KSEL(SEG_KDATA);
	tf -> cs = KSEL(SEG_KCODE);
	tf -> ss = KSEL(SEG_KDATA);
	tf -> eflags = 0x00000202;
}

void TrapFrame_user_init(TrapFrame *tf)
{
    tf -> ds = USEL(SEG_UDATA);
    tf -> es = USEL(SEG_UDATA);
    tf -> cs = USEL(SEG_UCODE);
    tf -> ss = USEL(SEG_UDATA);
    tf -> eflags = 0x00000202;
}

//PCBPool init
//进程池的初始化
void PCBPool_init()
{
     int i;
    for(i=0;i<PCB_Pool_NUM-1;i++)
    {
        PCBPool[i].next=&PCBPool[i+1];
		PCBPool[i].pgdir=NULL;
		PCBPool[i].tf = (void *)&kstack[i];
		PCBPool[i].type=UN_T;
    }
	PCBPool[i].next=NULL;
	PCBPool[i].pgdir=NULL;
	PCBPool[i].tf = (void *)&kstack[i];
    pcb_free = &PCBPool[0];
    current = NULL;
}

//创建线程/进程的基本框架 
PCB* new_PCB()
{
	PCB *pcb;
	if(pcb_free == NULL)
	{
		printk("\nNew PCB Failed!\nThe PCB pool is full!\n");
        return NULL;
	}
    pcb = pcb_free;
    pcb_free = pcb_free -> next;
    pcb->next = NULL;
	if(current == NULL)
        current = pcb;
    else
    {
        PCB *temp = current;
        while(temp->next != NULL)
            temp = temp -> next;
        temp -> next = pcb;
    }
    return pcb;
}


//extern PDE kpgdir[NR_PDE] PG_ALIGN;//the kernel page directory
//设置进程的地址空间
static int process_setup_vm(PCB *pcb)
{
	int res;
	Page *page = NULL;
	res = page_alloc(&page,GFP_KERNEL);
	/*GFP_KERNEL表示分配的物理空间在16M以内，GFP_USER表示分配的物理内存
	   在16M以外
	   在设置进程地址空间时，需要将内核的3G以上的地址空间赋给用户地址线性空
	   间的3G以上*/

	if(res < 0)
	    return res;
    
    memcpy(page2kva(page), kpgdir , PG_SIZE);
    pcb -> pgdir = (void *)page2pa(page);

	printk("process_setup_vm is here working! %s %d\n",__FILE__,__LINE__);
	return 0;
}

//创建一个进程
int process_fork(PCB **new_pcb)
{
    *new_pcb=new_PCB();
    TrapFrame_user_init((*new_pcb) -> tf);
    (*new_pcb)->type=PROC_T;
    return 0;
}

//从内核空间到用户空间的返回
void pcb_pop_tf(TrapFrame *tf)
{	
	//printk("pcb_pop_tf here is working! %s %d\n",__FILE__,__LINE__);
	asm volatile("movl %0,%%esp": :"g"(tf):"memory");
	asm volatile("popal");
	asm volatile("popl %%gs"::);
	asm volatile("popl %%fs"::);
	asm volatile("popl %%es"::);
	asm volatile("popl %%ds"::);
	asm volatile("addl $0x8,%%esp"::);
	asm volatile("iret");

	printk("pcb_pop_tf failed! %s %d\n",__FILE__,__LINE__);
	panic("iret failed!\n");

}

//Alloc len bytes from the address va
//从虚拟地址va开始分配len字节长度的空间
static void segment_alloc(PCB *pcb,void *va , size_t len)
{
	int i;
	Page *page;

	va =(void *)(PGROUNDDOWN(((long)(va))));
	printk("\n------------------------\nsegment_alloc is here working!va = 0x%x %s %d\n",va,__FILE__,__LINE__);
	for(i = 0;i < PGROUNDUP(len)/PG_SIZE;i++)
	{
		if(page_alloc(&page,GFP_USER) != 0)
		{
			printk("Segment_alloc page_alloc failed! %S %d\n",\
					__FILE__,__LINE__);
			return;
		}

		printk("segment_alloc page_alloc here working! %s %d\n",__FILE__,__LINE__);
		if(page_insert(pcb->pgdir,page,va + i*PG_SIZE,PTE_U | PTE_W) != 0)
		{
			printk("Segment_alloc page_insert failed! %S %d\n",\
				__FILE__,__LINE__);
			return;
		}
		printk("segment_alloc page_insert here working! %s %d\n",__FILE__,__LINE__);
	}

	//printk("segment_alloc is here working! %s %d\n",__FILE__,__LINE__);
	return;
}

//将用户进程的二进制文件直接从磁盘上加载到物理内存上
static void process_load(PCB *pcb)
{
   struct ELFHeader *elf;
    struct ProgramHeader *ph, *eph;
    unsigned char *pa, *i;

    process_setup_vm(pcb);
    set_cr3(pcb -> pgdir);
    elf = (struct ELFHeader*)0x8000;
    readseg((unsigned char*)elf, PG_SIZE, 67072);

    ph = (struct ProgramHeader*)((char *)elf + elf -> phoff);
    eph = ph + elf -> phnum;
    printk("loading elf\n");
    for(;ph < eph; ph++)
    {
        pa = (unsigned char*) ph -> paddr;
        segment_alloc(pcb, (void *)pa, PG_SIZE);
        readseg((unsigned char*)pa, ph -> filesz, ph -> off +67072);
        for(i = (unsigned char*)(pa + ph -> filesz);i < (unsigned char*)(pa + ph -> memsz); *i++=0);
    }
    segment_alloc(pcb,(void *)(USTACKTOP - 4096*(pcb->pid-9)), 4096);
    pcb -> tf -> esp = USTACKTOP - 4096;
    pcb -> tf -> eip = elf -> entry;
 /*Lab2 code here
   从磁盘中加载程序，具体的加载方法需要参考boot/main.c中加载内核的方法
   加载之前需要利用segment_alloc函数为加载的内容分配线性空间（同时也分配
   了物理空间），最后加载完程序之后，需要为用户进程创建一个4KB大小的用户
   栈*/
      printk("process_load is here working! %s %d\n",	__FILE__,__LINE__);
}

//在内核空间创建一个用户进程，并从此跳转到用户空间执行此进程pcb of idle
int enter_process_userspace(void)
{
	PCB *pcb;
	int res;

	printk("enter_process_userspace is working %s %d\n",\
			__FILE__,__LINE__);
	res = process_fork(&pcb);
	
	if(res < 0)
		return -1;

	printk("enter_process_userspace process_fork is working %s %d \n",\
			__FILE__,__LINE__);
    	pcb->pid=10;
	process_load(pcb);
	printk("enter_process_userspace process_load is working %s %d\n",\
			__FILE__,__LINE__);

	/*process_exec(pcb);
	printk("enter_process_userspace process_exec is working %s %d\n",\
			__FILE__,__LINE__);
*/
	wakeup(pcb);
	printk("wake up user pcb %s %d \n",__FILE__,\
			__LINE__);
    
	return 0;
}
PCB* create_kthread(void *fun){
	PCB *pcb=new_PCB();
	TrapFrame_init(pcb -> tf);
	printk("create kernel thread is working %s %d\n",__FILE__,__LINE__);
	pcb->tf->eip=(uint32_t)fun;
	pcb->pid=pid_pool;
	pid_pool++;
	pcb->type=THREAD_T;
	pcb->need_resched=1;
	return pcb;
}
//让用户进程执行
void process_exec(PCB *pcb)
{
	cli();
	current=pcb;
	//printk("exec pid =%d\n",current->pid);
	// printk("\nprogram %d entry is 0x%x\n",pcb->pid,pcb->tf->eip);
	if (pcb->type==THREAD_T){
			tss_set_esp0((uint32_t)pcb->tf);
			//current->need_resched=2;
			pcb_pop_tf(pcb -> tf);
			pcb->state=TASK_RUNNING;
	}
	else{
	pcb->state=TASK_RUNNING;
	set_cr3(pcb->pgdir);
	tss_set_esp0((uint32_t)pcb->tf);
    	pcb_pop_tf(pcb -> tf);
    }
}
void wakeup_all(){
	while(!list_empty(block_queue))
	wakeup(list_entry(block_queue,PCB,list));
}
void sys_fork()
{
	PCB *pcb=new_PCB();
	process_fork(&pcb);
	pcb->pid=11;
	pcb -> pgdir=current->pgdir;
	pcb->tf->eip=current->tf->eip;
	pcb->tf->esp=USTACKTOP - 4096*(pcb->pid-9);
	pcb->tf->eax=0;
	set_cr3(pcb->pgdir);
	segment_alloc(pcb,(void *)pcb->tf->esp, 4096);
	memcpy((void *)pcb->tf->esp,(void *)current->tf->esp,32);
	wakeup(pcb);
}