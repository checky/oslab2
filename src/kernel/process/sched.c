#include "sched.h"
#include "process.h"
//初始化就绪队列与阻塞队列
void queue_init(){
	ready_queue=&idle->list;
	block_queue=&block_body;
	list_init(ready_queue);
	list_init(block_queue);
	time_table=0;
	intr_flag=0;
}
PCB* choose_next_process(){
	PCB *next=list_entry(ready_queue->next,PCB,list);
	if(next->pid<0) panic("maybe out of meme");
	//printk("schedule... the next pid =%d eip=%x\n",next->pid,next->tf->eip);	
	return next;
}
void sleep(){
	cli();
	current->state=TASK_WAITING;
	list_del(&current->list);
	list_add_after(block_queue,&current->list);
	//printk("sleep thread pid=%d\n",current->pid);
	asm volatile("int $0x14");
}
void wakeup(PCB* pcb){
	cli();
	list_del(&pcb->list);
	list_add_after(ready_queue,&pcb->list);
	//printk("wake up thread pid=%d\n",pcb->pid);
	if(pcb->state!=TASK_WAITING) pcb->state=TASK_READY;
	pcb->time_count=1;
}
void schedule(){
	PCB *next=choose_next_process();
	//printk("next %d\n",next->pid);
	list_del(&next->list);
	list_add_before(ready_queue,&next->list);
	next->time_count=1;
	process_exec(next);
}