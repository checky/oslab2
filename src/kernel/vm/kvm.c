#include "vm.h"
#include "x86.h"
#include "string.h"

#define PG_ALIGN __attribute((aligned(PG_SIZE)))
PDE kpgdir[NR_PDE] PG_ALIGN;            // the kernel page directory
PTE kpgtab[MEM_MAX / PG_SIZE] PG_ALIGN; // the kernel page tables
SegDesc gdt[NR_SEG];                   // the new GDT
static TSS tss;                        // one TSS for all ring 3 processes


inline PDE* get_kpdir()
{
	return kpgdir;
}

inline PTE* get_kptable()
{
	return kpgtab;
}


//设置内核内存的映射
void init_kvm(void) 
{ // setup kernel memory mapping
	uint32_t pa;
	uint32_t cr0;

	PTE *pgtab = (PTE*)va_to_pa(kpgtab);
	PDE *pgdir = (PDE*)va_to_pa(kpgdir);
	for (pa = 0; pa < MEM_MAX; pa += PG_SIZE) {
		*pgtab = PTE_P | PTE_W | pa;
		pgtab ++;
	}
  	memset((void*)va_to_pa(pgdir), 0, PG_SIZE);
	for (pa = 0; pa < MEM_MAX; pa += PG_SIZE * NR_PTE) {
		*(pgdir + KOFFSET / PG_SIZE / NR_PTE) = *pgdir =
			PTE_P | PTE_W | va_to_pa(&kpgtab[pa / PG_SIZE]);
		pgdir ++;
	}
	set_cr3((void*)va_to_pa(kpgdir));  // set PDBR(CR3)
	cr0 = get_cr0();
//	cr0 |= CR0_PE | CR0_PG | CR0_AM | CR0_WP | CR0_NE | CR0_MP;
//	cr0 &= ~(CR0_TS | CR0_EM);
	cr0 |= CR0_PG;
	set_cr0(cr0); // enable paging
}

void tss_set_esp0(uint32_t esp)
{
	tss.esp0 = esp;
}

void
init_seg() { // setup kernel segements
	gdt[SEG_KCODE] = SEG(STA_X | STA_R, 0,       0xffffffff, DPL_KERN);
	gdt[SEG_KDATA] = SEG(STA_W,         0,       0xffffffff, DPL_KERN);
	gdt[SEG_UCODE] = SEG(STA_X | STA_R, 0,       0xffffffff, DPL_USER);
	gdt[SEG_UDATA] = SEG(STA_W,         0,       0xffffffff, DPL_USER);
	gdt[SEG_TSS] = SEG16(STS_T32A,      &tss, sizeof(TSS)-1, DPL_KERN);
    gdt[SEG_TSS].s = 0;
	set_gdt(gdt, sizeof(gdt));

	tss.ss0 = KSEL(SEG_KDATA);
	set_tr(KSEL(SEG_TSS));
    /*
	   Lab2 code here
	   初始化TSS
	 */
    asm volatile("movw %%ax,%%gs":: "a" (USEL(SEG_UDATA)));
	asm volatile("movw %%ax,%%fs":: "a" (USEL(SEG_UDATA)));

	asm volatile("movw %%ax,%%es":: "a" (KSEL(SEG_KDATA)));
	asm volatile("movw %%ax,%%ds":: "a" (KSEL(SEG_KDATA)));
	asm volatile("movw %%ax,%%ss":: "a" (KSEL(SEG_KDATA)));

	asm volatile("ljmp %0,$1f\n 1:\n":: "i"(KSEL(SEG_KCODE)));
//	asm volatile("movw %%ax,%%cs":: "a" (KSEL(SEG_KCODE)));

	//asm volatile("ldt $0");
	lldt(0);
}

Page phy_pages[32*1024] PG_ALIGN;
Page *pages;//Virtual address of physical page array

ListHead pages_free_list;//Free list of physical pagesa

void page_init(void)
{
	int i;
	pages = phy_pages;
	list_init(&pages_free_list);

	for(i = 0; i < 32*1024; i++)
	{
		pages[i].pp_ref = 0;
		pages[i].flag = 0;
	}


	//内核地址空间，用户空间内存分配不能在此处进行分配
	for(i = 0; i < 3*1024;i++)
	{
		pages[i].pp_ref = 1;
		pages[i].flag = 1;
	}

	printk("page_init is here working! %s %d\n",__FILE__,__LINE__);
	return;
}


int page_alloc(Page **page,int order)
{
	int i;
	int start,end;
	
	i = 0;

	if(order == GFP_KERNEL)
	{
		start = 3*1024;
		end = 4*1024;
	}
	else if(order == GFP_USER) 
	{
		 start = 4*1024;
		 end = 32*1024;
	}
	else
	{
		return -1;
	}

	for(i = start ; i < end ; i++)
	{
		if(order == GFP_USER && pages[i].flag == 0)
		{
			pages[i].pp_ref ++;
//			*page = &pages[i];
			pages[i].flag = 1;
			*page = &pages[i];

			printk("page_alloc USER is here working! %s %d\n",\
					__FILE__,__LINE__);
			return 0;
		}
		
		if(order==GFP_KERNEL && pages[i].flag == 0)
		{
			pages[i].pp_ref ++;
		//	*page = &pages[i];
			pages[i].flag = 1;
			*page = &pages[i];

			printk("page_alloc KERNEL is here working! %s %d\n",\
					__FILE__,__LINE__);
			return 0;
		}
	}
 
	return -1;
}

//释放物理页，其引用减一，为零时，标记也将设置为0
void page_free(Page *page)
{
	if(page->pp_ref == 0 && page->flag == 0)
		return;
	page->pp_ref--;

	if(page->pp_ref == 0)
	{
		page->flag = 0;
	}

	printk("page_init is here working! %s %d\n",__FILE__,__LINE__);
	return;
}
//完成虚拟地址到物理地址的映射，需要知晓页目录地址，相应的页描述符线性地址
int page_insert(PDE *pgdir, Page *page,void *va,int perm)
{
	//Fill this function
	PTE *pgtab;	
	pgtab = pgdir_walk(pgdir,va,1);
	printk("page_insert is here working! %s %d\n",__FILE__,__LINE__);

	if(pgtab == NULL)
	{
		printk("page_insert is here working! %s %d\n",__FILE__,__LINE__);
		return -1;
	}
	else
	{
		page -> pp_ref ++;
		if(( *pgtab & PTE_P ) != 0)
		{
			page_remove(pgdir,va);
		}

		*pgtab = page2pa(page) | PTE_P | perm;
	}

	printk("page_insert is here working! %s %d\n",__FILE__,__LINE__);
	return 0;
}
//清除物理页，相当于在页表中把相应的项清零
void page_remove(PDE *pgdir,void *va)
{
	Page *page;

	PTE *pgtab;
	page= page_lookup(pgdir,va, &pgtab);
	printk("page_remove page_lookup is here working! %s %d\n",\
			__FILE__,__LINE__);

	if(page == NULL)
	{
		return;
	}
	else
	{
		page_decref(page);
	}

	if(pgtab != NULL)
	{
		*pgtab = 0;
	}

	printk("page_remove is here working! %s %d\n",__FILE__,__LINE__);
	//tlb_invalidate(pgdir,va);
}


//根据线性地址，返回其对应物理页的页描述符的线性地址
Page *page_lookup(PDE *pgdir,void *va,PTE **pgtable)
{
	PTE *pgtab;
	pgtab = pgdir_walk(pgdir,va,0);
	if(pgtab == NULL)
	{
		return 0;
	}

	if(pgtable != NULL)
	{
		*pgtable = pgtab;
	}

	printk("page_lookup is here working! %s %d\n",__FILE__,__LINE__);

	return pa2page(*pgtab);
}
//减少物理页的引用
void page_decref(Page *page)
{
	if(--page->pp_ref == 0)
	{
		page_free(page);
	}

	printk("page_decref is here working! %s %d\n",__FILE__,__LINE__);
}

//返回虚拟地址对应的二级页表的页表项
PTE *pgdir_walk(PDE *pgdir, void *va,int create)
{
	PTE *pt_addr_v;
	Page *page;

	if((pgdir[PDX(va)] & PTE_P) != 0)
	{
		pt_addr_v = (PTE *)pa_to_va(PTE_ADDR(pgdir[PDX(va)]));
		printk("pgdir_walk is here working! %s %d\n",__FILE__,__LINE__);
		return &pt_addr_v[PTX(va)];
	}
	else
	{
		if(create == 0)
		{
			printk("pgdir_walk create = 0 is here working! %s %d\n",\
					__FILE__,__LINE__);
			return NULL;
		}
		else
		{
			if(page_alloc(&page,GFP_KERNEL) != 0)
			{
				printk("pgdir_walk is here working! %s %d\n",\
						__FILE__,__LINE__);
				return NULL;
			}
			else
			{
				page->pp_ref = 1;

				memset((void *)(pa_to_va(page2pa(page))), 0, PG_SIZE);
				pgdir[PDX(va)] = page2pa(page);
				pgdir[PDX(va)] = pgdir[PDX(va)] | PTE_U | PTE_W | PTE_P;

				pt_addr_v = (PTE *)pa_to_va(PTE_ADDR(pgdir[PDX(va)]));

				printk("page_dir_walk is here working! %s %d\n",\
						__FILE__,__LINE__);
				return &pt_addr_v[PTX(va)];
			}
		}
	}
}
//初始化页表，
void i386_vm_init(void)
{
	page_init();
	printk("i386_vm_init page_init is here working! %s %d\n",\
			__FILE__,__LINE__);
}
