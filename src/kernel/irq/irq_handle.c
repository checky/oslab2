#include "x86.h"
#include "kernel.h"
#include "syscall.h"

#include "sched.h"
//#include "adt/list.h"
#define USTACKTOP 0xbfffffff
/*static int stack_allow(PCB *pcb){
	if(pcb->type==THREAD_T){
		if(pcb->tf->esp>(uint32_t)pcb->tf+128)return 0;
		if(pcb->tf->esp<0)return 0;
	}
	else{
		if(pcb->tf->esp>USTACKTOP - 4096*(pcb->pid-10)-128)return 0;
		if(pcb->tf->esp<0)return 0;
	}

	return 1;
}*/
void irq_handle(TrapFrame *tf) 
{
 	//printk("irq=%d\n",tf->irq);	
	cli();
	//printk("=%x\n",tf->eip);
	int irq = tf->irq;
	/*if(current->pid==10){
	//printk("before esp %x current %x\n",current->tf->esp,tf->esp);
	printk("\nin trap\n");
	print_stack(tf);
	printk("current\n");
	print_stack(current->tf);
}*/
	//printk("%x",switch_esp);
	current->tf=tf;
	//if(!stack_allow(current)) panic("out of stack");
	//printk("1trap esp %x\n",current->tf->esp);
	//printk("irq=%d\n",tf->irq);
	// save the trap frame pointer for the old process
	assert(irq >= 0 || irq == -1);
 	if(irq == 1000||irq==20){
          
        	 current->time_count--;
	   //irq =1000 表示时钟中断
	   if (current->time_count <= 0)//time_count进程的时间片
            {
	    	schedule();
            } 
        }

	if (irq < 1000 && irq != 0x80 && irq != 20) 
	{
		// exception
		cli();
		printk("\nin function %d\n",current->pid);
		printk("Here is working (Function irq_handle())\n");
		printk("Unexpected exception #%d\n", irq);
		printk("Errorcode %x\n", tf->err);
		printk("Location  %d:%x, esp %x\n", tf->cs, tf->eip, tf->esp);
		panic("unexpected exception");

	} 
	else if(irq == 0x80)
	{
		uint32_t syscallno ; 
		uint32_t a1 ; 
		uint32_t a2 ; 
		uint32_t a3 ; 
		uint32_t a4 = 0; 
		uint32_t a5 = 0; 
		syscallno = tf->eax;
		a1 = tf->ebx;
		a2 = tf->ecx;
		a3 = tf->edx;
		asm volatile("movl %%esp , %0" : "=a"(tf)); 
	 	syscall(syscallno , a1 , a2 , a3 , a4 , a5);
		return;
	}
	
	
}

