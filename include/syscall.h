#ifndef _SYSCALL_H_
#define _SYSCALL_H_

#include "common.h"

/*system call numbers*/
enum 
{
	SYS_cputs = 0,
	SYS_cgetc,
	SYS_getpid,
	SYS_pcb_destroy,
	SYS_page_alloc,
	SYS_page_map,
	SYS_page_unmap,
	SYS_exofork,
	SYS_set_status,
	SYS_set_trapframe,
	SYS_set_pgfault_upcall,
	SYS_yield,
	SYS_ipc_try_send,
	SYS_ipc_recv,
    NSYSCALLS
};

int syscall(uint32_t syscallno, uint32_t a1, uint32_t a2, \
		uint32_t a3, uint32_t a4, uint32_t a5);
#endif
