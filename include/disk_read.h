#ifndef __DISK_READ_H__
#define __DISK_READ_H__

#include "x86.h"
#include "vm.h"
#include "elf.h"
//read a sector from the disk;
void readseg(unsigned char *,int ,int );

#endif
