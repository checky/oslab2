#ifndef __VM_H__
#define __VM_H__

#include "x86.h"
#include "debug.h"
#include "adt/list.h"
#include "types.h"
#include "kernel.h"

#define KOFFSET         0xC0000000
/*the maxinum kernel size is 16MB*/
#define KMEM            (16*1024*1024)
/*This system has 128MB physical memory*/
#define MEM_MAX         (128 * 1024 * 1024)
#define GFP_KERNEL      0
#define GFP_USER		3

#define va_to_pa(addr)  (((uint32_t)(addr)) - KOFFSET)
#define pa_to_va(addr)  (((uint32_t)(addr)) + KOFFSET)

inline CR3* get_kcr3();
inline PDE* get_kpdir();
inline PTE* get_kptable();

void make_invalid_pde(PDE *);
void make_invalid_pte(PTE *);
void make_pde(PDE *, void *);
void make_pte(PTE *, void *);

void init_kvm(void);
void init_seg(void);


typedef struct Page
{
	ListHead pages;
	uint16_t pp_ref;//表示此物理页被引用的次数
	unsigned flag;//flag=0 表示此物理页可被直接分配， 
	              //flag=1 表示此物理页可以被重复映射，但是不能被直接分配
}Page;

extern Page *pages;

int page_alloc(Page **page,int order);
int page_insert(PDE *pgdir,Page *page,void *va,int perm);

void page_init(void);
void page_free(Page *page);
void page_remove(PDE *pgdir,void *va);
void i386_vm_init(void);
void page_decref(Page *page);
Page *page_lookup(PDE *pgdir,void *va,PTE **pgtable);
PTE *pgdir_walk(PTE *pgdir , void *va ,int create);

void tss_set_esp0(uint32_t esp);

typedef uint32_t physaddr_t;
//根据页框描述符的地址，返回对应的页框号
static inline unsigned int page2ppn(Page *page)
{
	return page - pages;
}
//根据页框描述符的地址，获取对应的页框号，之后去得其物理地址
static inline physaddr_t page2pa(Page *page)
{
	return page2ppn(page) << PGSHIFT;
}

#define PPN(la)   (((uint32_t)(la)) >> 12)
//根据物理地址，获取对应页框描述符的地址
static inline Page *pa2page(physaddr_t pa)
{
	if(PPN(pa) >= (32 * 1024))
		panic("pa2page called with invalid pa");

	return &pages[PPN(pa)];
}
//根据页框描述符的地址，获取其在内核空间的线性地址
static inline void  *page2kva(Page *page)
{
	return (void *)(pa_to_va(page2pa(page)));
}

#endif
