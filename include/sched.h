#ifndef __SCHED_TEST_H__
#define __SCHED_TEST_H__
#include "process.h"
ListHead* ready_queue,  *block_queue;//就绪队列与阻塞队列
ListHead block_body;
int intr_flag;
extern PCB* idle;
int time_table;
void queue_init();
void sleep(void);
void schedule();
void wakeup(PCB* pcb);
PCB* choose_next_process();
#endif