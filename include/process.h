#ifndef __PROCESS_H__
#define __PROCESS_H__

#include "common.h"
#include "x86.h"
#include "adt/list.h"
#include "disk_read.h"
#include "vm.h"
#include "debug.h"
#include "string.h"


#define STK_SZ  8192
#define PCB_Pool_NUM 128

#define TASK_RUNNING         1
#define TASK_READY           2
#define TASK_INTERRUPTIBLE   3
#define TASK_UNINTERRUPTIBLE 4
#define TASK_WAITING 5
#define TASK_STOPPED         0
#define PG_ALIGN __attribute((aligned(PG_SIZE)))

extern PDE kpgdir[NR_PDE];


#define KSTACK_SIZE 4096


typedef struct PCB
{
	/*定义PCB的数据结构*/
	int pid;
	TrapFrame *tf;
	void *pgdir;
	struct PCB *next;
	ListHead list;
	int time_count;
	int type;
	int state;
	int need_resched;
}PCB;
enum PCB_type
{
	THREAD_T,PROC_T,UN_T
};

PCB *current ;
PCB *pcb_of_idle ;

uint8_t kstack[PCB_Pool_NUM][KSTACK_SIZE];
PCB PCBPool[PCB_Pool_NUM] PG_ALIGN;
void TrapFrame_init(TrapFrame *);

void PCBPool_init();
PCB* new_PCB();

int enter_process_userspace(void);

void process_exec(PCB *pcb);
int process_fork(PCB **new_pcb);
void print_stack(TrapFrame *tf);
PCB* create_kthread(void *fun);
void thread_testfunc();
void wakeup_all();
void queue_init();
void sys_fork();
#endif
