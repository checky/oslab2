#ifndef __COMMON_H__
#define __COMMON_H__

#include "const.h"
#include "types.h"

#define INTR assert(readf() & FL_IF)
#define NOINTR assert(~readf() & FL_IF)

#endif
