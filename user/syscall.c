//System call

//#include "syscall.h"
#include "lib.h"

#define T_SYSCALL   0x80

int SYS_PUTNO=0;
int SYS_FORKNO=7;
static inline int32_t syscall(int num, int check, uint32_t a1,uint32_t a2,
		uint32_t a3, uint32_t a4, uint32_t a5)
{
	int32_t ret = 0;
	//Generic system call: pass system call number in AX
	//up to five parameters in DX,CX,BX,DI,SI
	//Interrupt kernel with T_SYSCALL
	//
	//The "volatile" tells the assembler not to optimize
	//this instruction away just because we don't use the
	//return value
	//
	//The last clause tells the assembler that this can potentially
	//change the condition and arbitrary memory locations
	if(num==0x80)
	asm volatile("int $0x80":"=eax"(ret):"eax"(a1),"ebx"(a2),"ecx"(a3),"edx"(a4));
	
	/*Lab2 code here
	  嵌入汇编代码，调用int $0x80
	 */

	return ret;
}

int sys_cputs(const char *s,uint32_t len)
{
	return syscall(T_SYSCALL,0,SYS_PUTNO,0,(uint32_t)s,len,0);
	/*Lab2 code here*/
}
int sys_fork(){
	int res=syscall(T_SYSCALL,0,SYS_FORKNO,0,0,0,0);
	return res;
}
int cputs(const char *s,uint32_t len)
{
	return sys_cputs(s,len);
}
int fork(){

	return sys_fork();
}